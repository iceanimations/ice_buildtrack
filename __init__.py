import hiero.core
import hiero.ui
import PySide.QtGui as QtGui
from hiero.ui.BuildExternalMediaTrack import BuildTrackActionBase
from hiero.ui.BuildExternalMediaTrack import BuildExternalMediaTrackAction
from hiero.ui.BuildExternalMediaTrack import buildExternalMediaTrackAction

import os



class CustomBuildTrackAction(BuildTrackActionBase):
    action_name = "Custom Build Track"

    def __init__(self):
        QtGui.QAction.__init__(self, self.action_name, None)
        self.triggered.connect(self.doit)
        self._processorPreset = None
        self._errors = []
        self._useMaxVersions = True
        buildExternalMediaTrackAction.addAction(self)

    def remove(self):
        buildExternalMediaTrackAction.removeAction(self)


class PrintSelectedItems(CustomBuildTrackAction):
    action_name = "Print Selected Items"

    def doit(self):
        if not hasattr(hiero.ui.activeView(), 'selection'):
            print "no selection found"
            return
        for sel in self.getTrackItems():
            if not isinstance(sel, hiero.core.Transition):
                print sel.name()


class BuildAnimationTrackDialog(QtGui.QDialog):
    _basepath = r'P:\external'
    _projnames = ["Al_Mansour_Season_02", "Captain_Khalfan"]
    _epDir = "02_production"
    _seqDir = "SEQUENCES"

    def __init__(self, selection, parent=None):
        super(BuildAnimationTrackDialog, self).__init__(parent)
        self.setWindowTitle("Build Animation Track")
        self.setSizeGripEnabled(True)

        self._exportTemplate = None
        mainlayout = QtGui.QVBoxLayout()

        self._selection = selection
        self.projectBox = QtGui.QComboBox()
        self.projectBox.addItems(self.getProjects())
        self._project = self.projectBox.currentText()

        self.episodeBox = QtGui.QComboBox()
        self.episodeBox.addItems(self.getProjectEpisodes(self._project))
        self._episode = self.episodeBox.currentText()

        self.sequenceBox = QtGui.QComboBox()
        self.sequenceBox.addItems(self.getEpisodeSequences(self._project,
            self._episode))
        self._sequence = self.sequenceBox.currentText()

        self.projectBox.currentIndexChanged.connect(self.projectChanged)
        self.episodeBox.currentIndexChanged.connect(self.episodeChanged)

        layout = QtGui.QHBoxLayout()
        layout.addWidget(QtGui.QLabel("project"))
        layout.addWidget( self.projectBox )
        mainlayout.addLayout(layout)
        layout = QtGui.QHBoxLayout()
        layout.addWidget(QtGui.QLabel("episode"))
        layout.addWidget( self.episodeBox )
        mainlayout.addLayout(layout)
        layout = QtGui.QHBoxLayout()
        layout.addWidget(QtGui.QLabel("sequence"))
        layout.addWidget( self.sequenceBox )
        mainlayout.addLayout(layout)


        # Add the standard ok/cancel buttons, default to ok.
        self._buttonbox = QtGui.QDialogButtonBox(
                QtGui.QDialogButtonBox.StandardButton.Ok |
                QtGui.QDialogButtonBox.StandardButton.Cancel)
        self._buttonbox.button(QtGui.QDialogButtonBox.StandardButton.Ok).setText("Build")
        self._buttonbox.button(QtGui.QDialogButtonBox.StandardButton.Ok).setDefault(True)
        self._buttonbox.button(QtGui.QDialogButtonBox.StandardButton.Ok).setToolTip(
                "Builds the selected entry in the export template. Only"
                "enabled if an entry is selected in the view above.")
        self._buttonbox.accepted.connect(self.acceptTest)
        self._buttonbox.rejected.connect(self.reject)
        mainlayout.addWidget(self._buttonbox)

        self.setLayout(mainlayout)

    def projectChanged(self, event):
        self._project = self.projectBox.currentText()
        self.episodeBox.clear()
        self.episodeBox.addItems(self.getProjectEpisodes(self._project))

    def episodeChanged(self, event):
        self._episode= self.episodeBox.currentText()
        self.sequenceBox.clear()
        self.sequenceBox.addItems(self.getEpisodeSequences(self._project,
            self._episode))

    def acceptTest(self):
        self._project = self.projectBox.currentText()
        self._episode = self.episodeBox.currentText()
        self._sequence= self.sequenceBox.currentText()
        if not self._project or not self._episode or not self._sequence:
            QtGui.QMessageBox.warning(self, "Build Animation Track",
                    "Valid sequence was not selected",
                    QtGui.QMessageBox.Ok)
        else:
            self.accept()

    def itemProject(self, item):
        if hasattr(item, 'project'):
            return item.project()
        elif hasattr(item, 'parent'):
            return self.itemProject(item.parent())
        else:
            return None

    def getProjects(self):
        validpaths = [(name, os.path.join(self._basepath, name)) for name in
                self._projnames]
        projects = [name for name, path in validpaths if os.path.exists(path) and
                os.path.isdir(path)]
        return projects

    def getProjectEpisodes(self, project):
        episodesDir = os.path.join(self._basepath, project, self._epDir)
        if (not project or not os.path.exists(episodesDir) or not
                os.path.isdir(episodesDir)):
            return []
        eps = [name for name in os.listdir(episodesDir) if
                os.path.isdir(os.path.join(episodesDir, name))]
        return eps

    def getEpisodeSequences(self, project='', episode=''):
        episodesDir = os.path.join(self._basepath, project, self._epDir)
        sequencesDir = os.path.join(episodesDir, episode, self._seqDir)
        if (not project or not os.path.exists(sequencesDir) or not
                os.path.isdir(sequencesDir)):
            return []
        seqs = [name for name in os.listdir(sequencesDir) if
                os.path.isdir(os.path.join(sequencesDir, name))]
        return seqs

    def getPreset(self):
        presets = hiero.core.taskRegistry.localPresets()
        for preset in presets:
            if 'Preview' in preset.name() and 'Man':
                self._preset = preset
                break
        self._exportTemplate = hiero.core.ExportStructure2()
        self._exportTemplate.restore( preset._properties()["exportTemplate"] )
        self._resolver = self._preset.createResolver()


class BuildAnimationTrack(CustomBuildTrackAction, BuildExternalMediaTrackAction):
    action_name = "Build Animation Track"

    def configure(self, project, selection):
        dialog = BuildAnimationTrackDialog(selection)
        self._trackItem = ' '.join(selection[0].parent().name(), 'Preview')

        if dialog.exec_():
            self._exportTemplate = dialog._exportTemplate
            self._processorPreset = dialog._preset

            leafs = self.getLeafs(self._exportTemplate)
            if not leafs:
                return False
            structureElement = leafs[0]
            self._elementPath = structureElement.path()
            self._elementPreset = structureElement.preset()

            resolver = hiero.core.ResolveTable()
            resolver.merge(dialog._resolver())
            resolver.merge(self._elementPreset.createResolver())
            self._resolver = resolver

            self._project = project
            return True

        return False

    def getLeafs(self, struct):
        def getLeafChildren(element):
            leafs = []
            for path, child in element.children():
                if not child.isLeaf():
                    leafs.extend(getLeafChildren(child))
                else:
                    leafs.append(child)
            return leafs
        return getLeafChildren(struct.rootElement())

    def trackName(self):
        self._trackName


if __name__ == '__main__':
    myaction = PrintSelectedItems()
    #myaction.remove()
    #del hcore.events._eventHandlers['kShowContextMenu']['kTimeline'][-1]
